use crate::{facing::Arc, facing::Facing, params::SensorRadius, point::Point};

pub struct Neighborhood {
    centre: Point,
    sensor: SensorRadius,
    arc: Option<Arc>,
    next: Point,
    width_from_centre: u32,
    finished: bool,
}

impl Neighborhood {
    // next() is the only required method
    pub fn new(centre: Point, sensor: SensorRadius, arc: Option<Arc>) -> Neighborhood {
        let mut next = Point { x: 0, y: 0 };
        let mut skipped_top_lines: u32 = 0;

        if centre.y > sensor.radius {
            next.y = centre.y - sensor.radius;
        } else {
            skipped_top_lines = sensor.radius - centre.y;
        }

        if centre.x > skipped_top_lines {
            next.x = centre.x - skipped_top_lines;
        }
        if sensor.rounded && sensor.radius > 0 && next.x > 0 {
            next.x -= 1;
        }

        let mut width_from_centre = skipped_top_lines;
        if sensor.rounded && centre.y != 0 && sensor.radius > 0 {
            width_from_centre += 1
        };

        let mut nh = Neighborhood {
            centre,
            sensor,
            arc,
            next,
            width_from_centre,
            finished: false,
        };

        if let Some(a) = arc {
            while !nh.finished && !Neighborhood::point_in_arc(nh.next, nh.centre, a) {
                nh.advance_next();
            }
        }

        nh
    }

    fn advance_next(&mut self) {
        if self.next.x < self.centre.x + self.width_from_centre {
            self.next.x += 1;
        } else {
            self.next.y += 1;
            let new_half_width =
                Neighborhood::half_width(self.sensor, self.next.y.abs_diff(self.centre.y));
            if new_half_width > 0 {
                self.width_from_centre = new_half_width - 1;
                self.next.x = if self.centre.x > self.width_from_centre {
                    self.centre.x - self.width_from_centre
                } else {
                    0
                };
            } else {
                self.finished = true;
            }
        }
    }

    // Width of a row from the centre with 1 being the centre only
    // row is relative to the middle with 0 being centre
    fn half_width(sensor: SensorRadius, row: u32) -> u32 {
        if row > sensor.radius {
            return 0;
        }
        let width = sensor.radius + 1 - row;
        if sensor.rounded && row != 0 {
            return width + 1;
        }
        width
    }

    fn point_in_arc(point: Point, centre: Point, arc: Arc) -> bool {
        let faces = if arc.tail {
            vec![arc.facing, arc.facing.behind()]
        } else {
            vec![arc.facing]
        };

        let mut inside_all = false;

        for f in faces {
            let inside = match f {
                Facing::East => {
                    if point.x < centre.x {
                        false
                    } else {
                        let diff = point.x - centre.x;
                        let width = point.y.abs_diff(centre.y);
                        if width > diff {
                            false
                        } else {
                            true
                        }
                    }
                }
                Facing::West => {
                    if point.x > centre.x {
                        false
                    } else {
                        let diff = centre.x - point.x;
                        let width = point.y.abs_diff(centre.y);
                        if width > diff {
                            false
                        } else {
                            true
                        }
                    }
                }
                Facing::North => {
                    if point.y > centre.y {
                        false
                    } else {
                        let diff = centre.y - point.y;
                        let height = point.x.abs_diff(centre.x);
                        if height > diff {
                            false
                        } else {
                            true
                        }
                    }
                }
                Facing::South => {
                    if point.y < centre.y {
                        false
                    } else {
                        let diff = point.y - centre.y;
                        let height = point.x.abs_diff(centre.x);
                        if height > diff {
                            false
                        } else {
                            true
                        }
                    }
                }
                Facing::NorthEast => {
                    if point.x >= centre.x && point.y <= centre.y {
                        true
                    } else {
                        false
                    }
                }
                Facing::NorthWest => {
                    if point.x <= centre.x && point.y <= centre.y {
                        true
                    } else {
                        false
                    }
                }
                Facing::SouthEast => {
                    if point.x >= centre.x && point.y >= centre.y {
                        true
                    } else {
                        false
                    }
                }
                Facing::SouthWest => {
                    if point.x <= centre.x && point.y >= centre.y {
                        true
                    } else {
                        false
                    }
                }
            };
            inside_all = inside_all || inside;
        }

        return inside_all;
    }
}

impl Iterator for Neighborhood {
    type Item = Point;

    fn next(&mut self) -> Option<Self::Item> {
        if self.finished {
            None
        } else {
            let response = self.next;
            match self.arc {
                None => self.advance_next(),
                Some(a) => loop {
                    self.advance_next();
                    if self.finished || Neighborhood::point_in_arc(self.next, self.centre, a) {
                        break;
                    }
                },
            }
            Some(response)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_neighbourhood_points_middle() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: false,
            },
            None,
        );

        let expected = vec![
            Point { x: 20, y: 18 },
            Point { x: 19, y: 19 },
            Point { x: 20, y: 19 },
            Point { x: 21, y: 19 },
            Point { x: 18, y: 20 },
            Point { x: 19, y: 20 },
            Point { x: 20, y: 20 },
            Point { x: 21, y: 20 },
            Point { x: 22, y: 20 },
            Point { x: 19, y: 21 },
            Point { x: 20, y: 21 },
            Point { x: 21, y: 21 },
            Point { x: 20, y: 22 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_neighbourhood_rounded_points_middle() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: true,
            },
            None,
        );

        let expected = vec![
            Point { x: 19, y: 18 },
            Point { x: 20, y: 18 },
            Point { x: 21, y: 18 },
            Point { x: 18, y: 19 },
            Point { x: 19, y: 19 },
            Point { x: 20, y: 19 },
            Point { x: 21, y: 19 },
            Point { x: 22, y: 19 },
            Point { x: 18, y: 20 },
            Point { x: 19, y: 20 },
            Point { x: 20, y: 20 },
            Point { x: 21, y: 20 },
            Point { x: 22, y: 20 },
            Point { x: 18, y: 21 },
            Point { x: 19, y: 21 },
            Point { x: 20, y: 21 },
            Point { x: 21, y: 21 },
            Point { x: 22, y: 21 },
            Point { x: 19, y: 22 },
            Point { x: 20, y: 22 },
            Point { x: 21, y: 22 },
        ];
        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_neighbourhood_points_left_edge() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 0, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: false,
            },
            None,
        );

        let expected = vec![
            Point { x: 0, y: 18 },
            Point { x: 0, y: 19 },
            Point { x: 1, y: 19 },
            Point { x: 0, y: 20 },
            Point { x: 1, y: 20 },
            Point { x: 2, y: 20 },
            Point { x: 0, y: 21 },
            Point { x: 1, y: 21 },
            Point { x: 0, y: 22 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_neighbourhood_rounded_points_top_edge() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 1 },
            SensorRadius {
                radius: 2,
                rounded: true,
            },
            None,
        );

        let expected = vec![
            Point { x: 18, y: 0 },
            Point { x: 19, y: 0 },
            Point { x: 20, y: 0 },
            Point { x: 21, y: 0 },
            Point { x: 22, y: 0 },
            Point { x: 18, y: 1 },
            Point { x: 19, y: 1 },
            Point { x: 20, y: 1 },
            Point { x: 21, y: 1 },
            Point { x: 22, y: 1 },
            Point { x: 18, y: 2 },
            Point { x: 19, y: 2 },
            Point { x: 20, y: 2 },
            Point { x: 21, y: 2 },
            Point { x: 22, y: 2 },
            Point { x: 19, y: 3 },
            Point { x: 20, y: 3 },
            Point { x: 21, y: 3 },
        ];
        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_neighbourhood_points_top_left() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 1, y: 1 },
            SensorRadius {
                radius: 2,
                rounded: false,
            },
            None,
        );

        let expected = vec![
            Point { x: 0, y: 0 },
            Point { x: 1, y: 0 },
            Point { x: 2, y: 0 },
            Point { x: 0, y: 1 },
            Point { x: 1, y: 1 },
            Point { x: 2, y: 1 },
            Point { x: 3, y: 1 },
            Point { x: 0, y: 2 },
            Point { x: 1, y: 2 },
            Point { x: 2, y: 2 },
            Point { x: 1, y: 3 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_neighbourhood_rounded_points_top_left() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 0, y: 0 },
            SensorRadius {
                radius: 2,
                rounded: true,
            },
            None,
        );

        let expected = vec![
            Point { x: 0, y: 0 },
            Point { x: 1, y: 0 },
            Point { x: 2, y: 0 },
            Point { x: 0, y: 1 },
            Point { x: 1, y: 1 },
            Point { x: 2, y: 1 },
            Point { x: 0, y: 2 },
            Point { x: 1, y: 2 },
        ];
        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_neighbourhood_zero() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 10, y: 10 },
            SensorRadius {
                radius: 0,
                rounded: false,
            },
            None,
        );
        assert_eq!(Some(Point { x: 10, y: 10 }), nh.next());
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_neighbourhood_zero_rounded() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 10, y: 10 },
            SensorRadius {
                radius: 0,
                rounded: true,
            },
            None,
        );
        assert_eq!(Some(Point { x: 10, y: 10 }), nh.next());
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_north() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: false,
            },
            Some(Arc {
                facing: Facing::North,
                tail: false,
            }),
        );

        let expected = vec![
            Point { x: 20, y: 18 },
            Point { x: 19, y: 19 },
            Point { x: 20, y: 19 },
            Point { x: 21, y: 19 },
            Point { x: 20, y: 20 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_west_rounded() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: true,
            },
            Some(Arc {
                facing: Facing::West,
                tail: false,
            }),
        );

        let expected = vec![
            Point { x: 18, y: 19 },
            Point { x: 19, y: 19 },
            Point { x: 18, y: 20 },
            Point { x: 19, y: 20 },
            Point { x: 20, y: 20 },
            Point { x: 18, y: 21 },
            Point { x: 19, y: 21 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_southeast() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: false,
            },
            Some(Arc {
                facing: Facing::SouthEast,
                tail: false,
            }),
        );

        let expected = vec![
            Point { x: 20, y: 20 },
            Point { x: 21, y: 20 },
            Point { x: 22, y: 20 },
            Point { x: 20, y: 21 },
            Point { x: 21, y: 21 },
            Point { x: 20, y: 22 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_northeast_rounded() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: true,
            },
            Some(Arc {
                facing: Facing::NorthEast,
                tail: false,
            }),
        );

        let expected = vec![
            Point { x: 20, y: 18 },
            Point { x: 21, y: 18 },
            Point { x: 20, y: 19 },
            Point { x: 21, y: 19 },
            Point { x: 22, y: 19 },
            Point { x: 20, y: 20 },
            Point { x: 21, y: 20 },
            Point { x: 22, y: 20 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }
    #[test]
    fn test_arc_east_tail() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: false,
            },
            Some(Arc {
                facing: Facing::East,
                tail: true,
            }),
        );

        let expected = vec![
            Point { x: 19, y: 19 },
            Point { x: 21, y: 19 },
            Point { x: 18, y: 20 },
            Point { x: 19, y: 20 },
            Point { x: 20, y: 20 },
            Point { x: 21, y: 20 },
            Point { x: 22, y: 20 },
            Point { x: 19, y: 21 },
            Point { x: 21, y: 21 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_south_tail_rounded() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: true,
            },
            Some(Arc {
                facing: Facing::South,
                tail: true,
            }),
        );

        let expected = vec![
            Point { x: 19, y: 18 },
            Point { x: 20, y: 18 },
            Point { x: 21, y: 18 },
            Point { x: 19, y: 19 },
            Point { x: 20, y: 19 },
            Point { x: 21, y: 19 },
            Point { x: 20, y: 20 },
            Point { x: 19, y: 21 },
            Point { x: 20, y: 21 },
            Point { x: 21, y: 21 },
            Point { x: 19, y: 22 },
            Point { x: 20, y: 22 },
            Point { x: 21, y: 22 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_northwest_tail() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: false,
            },
            Some(Arc {
                facing: Facing::NorthWest,
                tail: true,
            }),
        );

        let expected = vec![
            Point { x: 20, y: 18 },
            Point { x: 19, y: 19 },
            Point { x: 20, y: 19 },
            Point { x: 18, y: 20 },
            Point { x: 19, y: 20 },
            Point { x: 20, y: 20 },
            Point { x: 21, y: 20 },
            Point { x: 22, y: 20 },
            Point { x: 20, y: 21 },
            Point { x: 21, y: 21 },
            Point { x: 20, y: 22 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_southwest_tail_rounded() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 20, y: 20 },
            SensorRadius {
                radius: 2,
                rounded: true,
            },
            Some(Arc {
                facing: Facing::SouthWest,
                tail: true,
            }),
        );

        let expected = vec![
            Point { x: 20, y: 18 },
            Point { x: 21, y: 18 },
            Point { x: 20, y: 19 },
            Point { x: 21, y: 19 },
            Point { x: 22, y: 19 },
            Point { x: 18, y: 20 },
            Point { x: 19, y: 20 },
            Point { x: 20, y: 20 },
            Point { x: 21, y: 20 },
            Point { x: 22, y: 20 },
            Point { x: 18, y: 21 },
            Point { x: 19, y: 21 },
            Point { x: 20, y: 21 },
            Point { x: 19, y: 22 },
            Point { x: 20, y: 22 },
        ];

        for e in expected {
            let acutal = nh.next();
            assert_eq!(Some(e), acutal);
        }
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_zero() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 10, y: 10 },
            SensorRadius {
                radius: 0,
                rounded: false,
            },
            Some(Arc {
                facing: Facing::East,
                tail: false,
            }),
        );
        assert_eq!(Some(Point { x: 10, y: 10 }), nh.next());
        assert_eq!(None, nh.next());
    }

    #[test]
    fn test_arc_zero_rounded_with_tail() -> () {
        let mut nh = Neighborhood::new(
            Point { x: 10, y: 10 },
            SensorRadius {
                radius: 0,
                rounded: true,
            },
            Some(Arc {
                facing: Facing::NorthEast,
                tail: true,
            }),
        );
        assert_eq!(Some(Point { x: 10, y: 10 }), nh.next());
        assert_eq!(None, nh.next());
    }
}
