use rand::distributions::{Distribution, Standard};
use rand::Rng;
use strum::EnumCount;
use strum_macros::EnumCount;

#[cfg(test)]
use std::fmt;

// Arcs with or without rounding
// 2 without:   X     2 with:  XXX
//             XXX            XXXXX
//            XXOXX           XXOXX
//             XXX            XXXXX
//              X              XXX
// North
// 2 without:   X     2 with:  XXX
//             XXX             XXX
//              O               O
// North East
// 2 without:   X     2 with:   XX
//              XX              XXX
//              OXX             OXX

#[derive(Clone, Copy)]
pub struct Arc {
    pub facing: Facing,
    pub tail: bool,
}

#[derive(Clone, Copy, EnumCount)]
pub enum Facing {
    North = 0,
    NorthEast = 1,
    East = 2,
    SouthEast = 3,
    South = 4,
    SouthWest = 5,
    West = 6,
    NorthWest = 7,
}

impl Facing {
    pub fn behind(&self) -> Facing {
        match *self {
            Facing::North => Facing::South,
            Facing::NorthEast => Facing::SouthWest,
            Facing::East => Facing::West,
            Facing::SouthEast => Facing::NorthWest,
            Facing::South => Facing::North,
            Facing::SouthWest => Facing::NorthEast,
            Facing::West => Facing::East,
            Facing::NorthWest => Facing::SouthEast,
        }
    }

    pub fn right(&self) -> Facing {
        match *self {
            Facing::North => Facing::East,
            Facing::NorthEast => Facing::SouthEast,
            Facing::East => Facing::South,
            Facing::SouthEast => Facing::SouthWest,
            Facing::South => Facing::West,
            Facing::SouthWest => Facing::NorthWest,
            Facing::West => Facing::North,
            Facing::NorthWest => Facing::NorthEast,
        }
    }

    pub fn left(&self) -> Facing {
        match *self {
            Facing::North => Facing::West,
            Facing::NorthEast => Facing::NorthWest,
            Facing::East => Facing::North,
            Facing::SouthEast => Facing::NorthEast,
            Facing::South => Facing::East,
            Facing::SouthWest => Facing::SouthEast,
            Facing::West => Facing::South,
            Facing::NorthWest => Facing::SouthWest,
        }
    }

    pub fn x(&self) -> i8 {
        match *self {
            Facing::North => 0,
            Facing::NorthEast => 1,
            Facing::East => 1,
            Facing::SouthEast => 1,
            Facing::South => 0,
            Facing::SouthWest => -1,
            Facing::West => -1,
            Facing::NorthWest => -1,
        }
    }

    pub fn y(&self) -> i8 {
        match *self {
            Facing::North => 1,
            Facing::NorthEast => 1,
            Facing::East => 0,
            Facing::SouthEast => -1,
            Facing::South => -1,
            Facing::SouthWest => -1,
            Facing::West => 0,
            Facing::NorthWest => 1,
        }
    }
}

impl Distribution<Facing> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Facing {
        match rng.gen_range(0..Facing::COUNT) {
            0 => Facing::North,
            1 => Facing::NorthEast,
            2 => Facing::East,
            3 => Facing::SouthEast,
            4 => Facing::South,
            5 => Facing::SouthWest,
            6 => Facing::West,
            _ => Facing::NorthWest,
        }
    }
}

#[cfg(test)]
impl fmt::Display for Facing {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let text = match *self {
            Facing::North => "North",
            Facing::NorthEast => "NorthEast",
            Facing::East => "East",
            Facing::SouthEast => "SouthEast",
            Facing::South => "South",
            Facing::SouthWest => "SouthWest",
            Facing::West => "West",
            Facing::NorthWest => "NorthWest",
        };
        write!(f, "{}", text)
    }
}
