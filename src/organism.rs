use rand::distributions::{Distribution, Standard};
use rand::Rng;
use std::cmp;
use strum::EnumCount;
use strum_macros::EnumCount;

use crate::facing::{Arc, Facing};
use crate::habitat::Habitat;
use crate::neighbourhood::Neighborhood;
use crate::params::Params;
use crate::point::Point;

#[cfg(test)]
use strum_macros::EnumIter;

// A temporary static to make identification of unused sensors
static INACTIVE_SENSOR_RESPONSE: f32 = 0.0;

#[derive(Clone, Copy, Debug, EnumCount, PartialEq)]
pub enum Action {
    MoveEast,
    MoveWest,
    MoveNorth,
    MoveSouth,
    MoveFwd,
    MoveX,
    MoveY,
    SetInvResponsiveness,
    SetOsc1,
    EmitSignal0,
    KillFwd,
    MoveReverse,
    MoveLeft,
    MoveRight,
    MoveSide,
    MoveRandom,
    SetLongprobeDist,
}

static NUM_ACTION_TYPES: u8 = Action::COUNT as u8;

impl Action {
    fn from_u8(i: u8) -> Action {
        match i % NUM_ACTION_TYPES {
            0 => Action::MoveEast,
            1 => Action::MoveWest,
            2 => Action::MoveNorth,
            3 => Action::MoveSouth,
            4 => Action::MoveFwd,
            5 => Action::MoveX,
            6 => Action::MoveY,
            7 => Action::SetInvResponsiveness,
            8 => Action::SetOsc1,
            9 => Action::EmitSignal0,
            10 => Action::KillFwd,
            11 => Action::MoveReverse,
            12 => Action::MoveLeft,
            13 => Action::MoveRight,
            14 => Action::MoveSide,
            15 => Action::MoveRandom,
            _ => Action::SetLongprobeDist,
        }
    }
}

#[cfg_attr(test, derive(EnumIter))]
#[derive(Clone, Copy, Debug, EnumCount, PartialEq)]
enum Sensor {
    Age,
    BoundaryDist,
    BoundaryDistX,
    BoundaryDistY,
    LastMoveDirX,
    LastMoveDirY,
    LocX,
    LocY,
    LongProbePopulationFwd,
    LongProbeBarrierFwd,
    ShortProbeBarrierFwd,
    ShortProbeBarrierLeftRight,
    Osc1,
    Population,
    PopulationFwd,
    PopulationSide,
    Random,
    Signal0,
    Signal0Fwd,
    Signal0Side,
    GeneticSimilarityFwd,
}

static NUM_SENSOR_TYPES: u8 = Sensor::COUNT as u8;

impl Sensor {
    fn from_u8(i: u8) -> Sensor {
        match i % NUM_SENSOR_TYPES {
            0 => Sensor::Age,
            1 => Sensor::BoundaryDist,
            2 => Sensor::BoundaryDistX,
            3 => Sensor::BoundaryDistY,
            4 => Sensor::LastMoveDirX,
            5 => Sensor::LastMoveDirY,
            6 => Sensor::LocX,
            7 => Sensor::LocY,
            8 => Sensor::LongProbePopulationFwd,
            9 => Sensor::LongProbeBarrierFwd,
            10 => Sensor::ShortProbeBarrierFwd,
            11 => Sensor::ShortProbeBarrierLeftRight,
            12 => Sensor::Osc1,
            13 => Sensor::Population,
            14 => Sensor::PopulationFwd,
            15 => Sensor::PopulationSide,
            16 => Sensor::Random,
            17 => Sensor::Signal0,
            18 => Sensor::Signal0Fwd,
            19 => Sensor::Signal0Side,
            _ => Sensor::GeneticSimilarityFwd,
        }
    }

    #[cfg(test)]
    fn to_u8(&self) -> u8 {
        match self {
            Sensor::Age => 0,
            Sensor::BoundaryDist => 1,
            Sensor::BoundaryDistX => 2,
            Sensor::BoundaryDistY => 3,
            Sensor::LastMoveDirX => 4,
            Sensor::LastMoveDirY => 5,
            Sensor::LocX => 6,
            Sensor::LocY => 7,
            Sensor::LongProbePopulationFwd => 8,
            Sensor::LongProbeBarrierFwd => 9,
            Sensor::ShortProbeBarrierFwd => 10,
            Sensor::ShortProbeBarrierLeftRight => 11,
            Sensor::Osc1 => 12,
            Sensor::Population => 13,
            Sensor::PopulationFwd => 14,
            Sensor::PopulationSide => 15,
            Sensor::Random => 16,
            Sensor::Signal0 => 17,
            Sensor::Signal0Fwd => 18,
            Sensor::Signal0Side => 19,
            Sensor::GeneticSimilarityFwd => 20,
        }
    }
}

#[derive(Clone, Copy, EnumCount)]
enum SinkType {
    Neuron = 0,
    Action = 1,
}

impl Distribution<SinkType> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> SinkType {
        match rng.gen_range(0..SinkType::COUNT) {
            0 => SinkType::Neuron,
            _ => SinkType::Action,
        }
    }
}

#[derive(Clone, Copy, EnumCount)]
enum SourceType {
    Neuron = 0,
    Sensor = 1,
}

impl Distribution<SourceType> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> SourceType {
        match rng.gen_range(0..SourceType::COUNT) {
            0 => SourceType::Neuron,
            _ => SourceType::Sensor,
        }
    }
}

static MAX_SOURCE_SINK_NUM: u8 = u8::pow(2, 7) - 1;

#[derive(Clone, Copy)]
struct Gene {
    source_type: SourceType,
    source_num: u8,
    sink_type: SinkType,
    sink_num: u8,
    weight: i16,
}

impl Gene {
    fn random() -> Gene {
        Gene {
            source_type: rand::random(),
            source_num: rand::thread_rng().gen_range(0..MAX_SOURCE_SINK_NUM),
            sink_type: rand::random(),
            sink_num: rand::thread_rng().gen_range(0..MAX_SOURCE_SINK_NUM),
            weight: rand::thread_rng().gen::<i16>(),
        }
    }

    fn gene_string(&self) -> String {
        let type_bit: u8 = (self.source_type as u8) << 7;
        let source: u8 = type_bit | self.source_num;
        let type_bit: u8 = (self.sink_type as u8) << 7;
        let sink: u8 = type_bit | self.sink_num;
        format!("{:0>2x}{:0>2x}{:0>4x}", source, sink, self.weight)
    }
}

enum SensorMatch {
    Barrier,
    Organism,
}

#[derive(Debug, PartialEq)]
enum SinkNode {
    Action(Action),
    Neuron(u8),
}

#[derive(Debug, PartialEq)]
enum SourceNode {
    Sensor(Sensor),
    Neuron(u8),
}

#[derive(Debug, PartialEq)]
struct NeuralNode {
    source: SourceNode,
    sink: SinkNode,
    weight: f32,
}

pub struct Organism {
    genome: Vec<Gene>,
    color: u32,
    facing: Facing,
    location: Point,
    neural_net: Vec<NeuralNode>,
    neuron_values: Vec<f32>,
    osc_period: u32,
}

impl Organism {
    pub fn new(location: Point, params: &Params) -> Organism {
        let mut genome: Vec<Gene> = Vec::with_capacity(params.genome_length);
        for _i in 0..params.genome_length {
            genome.push(Gene::random());
        }

        let neural_net = Organism::build_neural_net(&genome, params);

        Organism {
            genome,
            color: 32000,
            facing: rand::random(),
            location,
            neural_net,
            neuron_values: vec![0.0; params.max_neurons as usize],
            osc_period: params.default_osc_period,
        }
    }

    pub fn color(&self) -> u32 {
        self.color
    }

    pub fn x(&self) -> u32 {
        self.location.x
    }

    pub fn y(&self) -> u32 {
        self.location.y
    }

    pub fn genome_string(&self) -> String {
        let mut genes = String::new();
        let mut prefix = "";

        for g in &self.genome {
            genes.push_str(&format!("{}{}", prefix, g.gene_string()));
            if prefix == "" {
                prefix = " ";
            }
        }

        genes
    }

    pub fn calc_action_priorities(
        &self,
        habitat: &Habitat,
        params: &Params,
        curr_step: u32,
    ) -> (Vec<(Action, f32)>, Vec<f32>) {
        let mut action_values: Vec<(Action, f32)> = Vec::new();
        let mut neuron_values = self.neuron_values.clone();

        for nn in &self.neural_net {
            let source_value = match &nn.source {
                SourceNode::Sensor(s) => self.sensor_value(*s, habitat, params, curr_step),
                SourceNode::Neuron(i) => neuron_values[*i as usize],
            };

            match &(nn.sink) {
                SinkNode::Action(a) => action_values.push((*a, source_value)),
                SinkNode::Neuron(i) => neuron_values[*i as usize] = source_value,
            };
        }

        return (action_values, neuron_values);
    }

    pub fn set_neuron_values(&mut self, new: &Vec<f32>) {
        self.neuron_values = new.clone();
    }

    fn build_neural_net(genes: &Vec<Gene>, params: &Params) -> Vec<NeuralNode> {
        let mut action_net = Vec::new();
        let mut net = Vec::new();
        for g in Organism::filter_unconnected_genes(genes, params.max_neurons) {
            let action: bool;
            let source = match g.source_type {
                SourceType::Sensor => SourceNode::Sensor(Sensor::from_u8(g.source_num)),
                SourceType::Neuron => SourceNode::Neuron(g.source_num % params.max_neurons),
            };
            let sink = match g.sink_type {
                SinkType::Action => {
                    action = true;
                    SinkNode::Action(Action::from_u8(g.sink_num))
                }
                SinkType::Neuron => {
                    action = false;
                    SinkNode::Neuron(g.sink_num % params.max_neurons)
                }
            };
            let node = NeuralNode {
                source,
                sink,
                weight: Organism::weight_as_float(g.weight),
            };
            if action {
                action_net.push(node);
            } else {
                net.push(node);
            }
        }
        net.append(&mut action_net);
        net
    }

    fn filter_unconnected_genes(genes: &Vec<Gene>, max_neurons: u8) -> Vec<Gene> {
        let mut action_neuron = vec![false; max_neurons as usize];
        let mut neuron_map = vec![u8::MAX; max_neurons as usize];

        for g in genes {
            match g.source_type {
                SourceType::Neuron => {
                    let source_neuron_id = g.source_num % max_neurons;
                    match g.sink_type {
                        SinkType::Action => {
                            action_neuron[source_neuron_id as usize] = true;
                        }
                        SinkType::Neuron => {
                            neuron_map[source_neuron_id as usize] = g.sink_num % max_neurons;
                        }
                    }
                }
                SourceType::Sensor => (),
            }
        }

        let mut filtered: Vec<Gene> = Vec::new();
        for g in genes {
            let mut insert = false;
            match g.sink_type {
                SinkType::Action => insert = true,
                SinkType::Neuron => {
                    let mut neuron_id = g.sink_num % max_neurons;
                    let mut count = 0;

                    loop {
                        if action_neuron[neuron_id as usize] == true {
                            insert = true;
                            break;
                        }
                        neuron_id = neuron_map[neuron_id as usize];
                        if neuron_id == u8::MAX || count > max_neurons {
                            break;
                        }
                        count += 1;
                    }
                }
            }

            if insert {
                filtered.push(*g);
            }
        }
        filtered
    }

    fn sensor_boundry_diff(max: u32, curr: u32) -> f32 {
        let mid = max / 2;
        let diff = if curr < mid { curr } else { max - curr - 1 };
        diff as f32 / (mid - 1) as f32
    }

    fn sensor_obstacle_dist(&self, habitat: &Habitat, facing: Facing, range: u32) -> u32 {
        let mut dist: u32 = 0;
        let mut sensor = self.location;

        loop {
            if !sensor.advance(facing) {
                break;
            }

            if habitat.is_barrier(sensor) {
                break;
            }

            dist += 1;
            if dist >= range {
                break;
            }
        }
        dist
    }

    fn sensor_long_probe(&self, habitat: &Habitat, facing: Facing, stop_at: SensorMatch) -> f32 {
        let mut dist: u32 = 0;
        let mut sensor = self.location;
        let mut max_range = false;

        loop {
            dist += 1;
            if !sensor.advance(facing) {
                max_range = true;
                break;
            }

            let result = match stop_at {
                SensorMatch::Barrier => habitat.probe_is_barrier(sensor),
                SensorMatch::Organism => habitat.probe_is_populated(sensor),
            };

            match result {
                Some(r) => {
                    if r {
                        break;
                    }
                }
                None => {
                    max_range = true;
                    break;
                }
            };
        }
        if max_range {
            1.0
        } else {
            dist as f32 / cmp::max(habitat.width, habitat.height) as f32
        }
    }

    fn sensor_population(&self, habitat: &Habitat, params: &Params, arc: Option<Arc>) -> f32 {
        let mut nh = Neighborhood::new(self.location, params.population_sensor_radius, arc);

        let mut cell_count: u32 = 0;
        let mut pop_count: u32 = 0;

        loop {
            let loc = nh.next();
            match loc {
                None => break,
                Some(p) => {
                    cell_count += 1;
                    if habitat.is_populated(p) {
                        pop_count += 1;
                    }
                }
            }
        }

        pop_count as f32 / cell_count as f32
    }

    fn sensor_value(
        &self,
        sensor: Sensor,
        habitat: &Habitat,
        params: &Params,
        curr_step: u32,
    ) -> f32 {
        match sensor {
            Sensor::Age => curr_step as f32 / params.steps_per_gen as f32,
            Sensor::BoundaryDist => {
                let x_diff = Organism::sensor_boundry_diff(habitat.width, self.location.x);
                let y_diff = Organism::sensor_boundry_diff(habitat.height, self.location.y);
                if x_diff < y_diff {
                    x_diff
                } else {
                    y_diff
                }
            }
            Sensor::BoundaryDistX => Organism::sensor_boundry_diff(habitat.width, self.location.x),
            Sensor::BoundaryDistY => Organism::sensor_boundry_diff(habitat.height, self.location.y),
            Sensor::LastMoveDirX => ((self.facing.x() + 1) as f32) / 2.0,
            Sensor::LastMoveDirY => ((self.facing.y() + 1) as f32) / 2.0,
            Sensor::LocX => self.location.x as f32 / (habitat.width - 1) as f32,
            Sensor::LocY => self.location.y as f32 / (habitat.height - 1) as f32,
            Sensor::LongProbePopulationFwd => {
                self.sensor_long_probe(habitat, self.facing, SensorMatch::Organism)
            }
            Sensor::LongProbeBarrierFwd => {
                self.sensor_long_probe(habitat, self.facing, SensorMatch::Barrier)
            }
            Sensor::ShortProbeBarrierFwd => {
                self.sensor_obstacle_dist(habitat, self.facing, params.barrier_sensor_radius) as f32
                    / params.barrier_sensor_radius as f32
            }
            Sensor::ShortProbeBarrierLeftRight => {
                let left = self.sensor_obstacle_dist(
                    habitat,
                    self.facing.left(),
                    params.barrier_sensor_radius,
                );
                let right = self.sensor_obstacle_dist(
                    habitat,
                    self.facing.right(),
                    params.barrier_sensor_radius,
                );
                cmp::min(left, right) as f32 / params.barrier_sensor_radius as f32
            }
            Sensor::Osc1 => f32::sin(curr_step as f32 / self.osc_period as f32),
            Sensor::Population => self.sensor_population(habitat, params, None),
            Sensor::PopulationFwd => {
                let arc = Arc {
                    facing: self.facing,
                    tail: false,
                };
                self.sensor_population(habitat, params, Some(arc))
            }
            Sensor::PopulationSide => {
                let arc = Arc {
                    facing: self.facing.right(),
                    tail: true,
                };
                self.sensor_population(habitat, params, Some(arc))
            }
            Sensor::Random => rand::thread_rng().gen(),
            Sensor::Signal0 => INACTIVE_SENSOR_RESPONSE,
            Sensor::Signal0Fwd => INACTIVE_SENSOR_RESPONSE,
            Sensor::Signal0Side => INACTIVE_SENSOR_RESPONSE,
            Sensor::GeneticSimilarityFwd => INACTIVE_SENSOR_RESPONSE,
        }
    }

    fn weight_as_float(weight: i16) -> f32 {
        (weight as f32) / (i16::MAX as f32)
    }
}

#[cfg(test)]
mod tests {
    use crate::params::SensorRadius;
    use strum::IntoEnumIterator;

    use super::*;

    fn genome_long_neuron_list() -> Vec<Gene> {
        // BoundaryDist -> N1 -> N2 -> N3 -> MoveNorth
        // BoundaryDist -> MoveWest
        vec![
            Gene {
                source_type: SourceType::Sensor,
                source_num: Sensor::BoundaryDist.to_u8(),
                sink_type: SinkType::Action,
                sink_num: 1,
                weight: i16::MIN,
            },
            // Long loop which is self referential but with action so valid
            Gene {
                source_type: SourceType::Sensor,
                source_num: Sensor::BoundaryDist.to_u8(),
                sink_type: SinkType::Neuron,
                sink_num: 1,
                weight: i16::MIN / 3,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 2,
                weight: 0,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 2,
                sink_type: SinkType::Neuron,
                sink_num: 3,
                weight: i16::MAX / 3,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 3,
                sink_type: SinkType::Action,
                sink_num: 2,
                weight: i16::MAX,
            },
        ]
    }

    #[test]
    fn test_sensor_u8_map() -> () {
        for i in 0..(NUM_SENSOR_TYPES * 2) {
            let sensor = Sensor::from_u8(i);
            assert_eq!(i % NUM_SENSOR_TYPES, sensor.to_u8());
        }
    }

    #[test]
    fn test_genome_string() -> () {
        let params = Params::default();
        let mut organism = Organism::new(Point { x: 0, y: 0 }, &params);
        organism.genome = vec![
            Gene {
                source_type: SourceType::Neuron,
                source_num: 12,
                sink_type: SinkType::Action,
                sink_num: 0,
                weight: 0,
            },
            Gene {
                source_type: SourceType::Sensor,
                source_num: 120,
                sink_type: SinkType::Neuron,
                sink_num: 14,
                weight: i16::MAX,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 24,
                sink_type: SinkType::Neuron,
                sink_num: MAX_SOURCE_SINK_NUM,
                weight: i16::MIN,
            },
        ];
        assert_eq!(organism.genome_string(), "0c800000 f80e7fff 187f8000");
    }

    #[test]
    fn test_action_ordering_no_neurons() -> () {
        let params = Params::default();
        let genome = vec![
            Gene {
                source_type: SourceType::Sensor,
                source_num: 1,
                sink_type: SinkType::Action,
                sink_num: 1,
                weight: i16::MIN,
            },
            Gene {
                source_type: SourceType::Sensor,
                source_num: 2,
                sink_type: SinkType::Action,
                sink_num: (NUM_ACTION_TYPES * 2) + 2,
                weight: i16::MIN / 2,
            },
            Gene {
                source_type: SourceType::Sensor,
                source_num: 3,
                sink_type: SinkType::Action,
                sink_num: (NUM_ACTION_TYPES * 3) + 3,
                weight: i16::MAX / 2,
            },
            Gene {
                source_type: SourceType::Sensor,
                source_num: 4,
                sink_type: SinkType::Action,
                sink_num: (NUM_ACTION_TYPES * 3) + 4,
                weight: i16::MAX,
            },
        ];
        let expected: Vec<NeuralNode> = vec![
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDist),
                sink: SinkNode::Action(Action::MoveWest),
                weight: -1.0000305,
            },
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDistX),
                sink: SinkNode::Action(Action::MoveNorth),
                weight: -0.50001526,
            },
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDistY),
                sink: SinkNode::Action(Action::MoveSouth),
                weight: 0.49998474,
            },
            NeuralNode {
                source: SourceNode::Sensor(Sensor::LastMoveDirX),
                sink: SinkNode::Action(Action::MoveFwd),
                weight: 1.0,
            },
        ];

        assert_eq!(Organism::build_neural_net(&genome, &params), expected);
    }

    #[test]
    fn test_action_ordering_valid_neurons() -> () {
        let params = Params::default();
        let genome = vec![
            Gene {
                source_type: SourceType::Sensor,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 1,
                weight: 0,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 1,
                sink_type: SinkType::Action,
                sink_num: 2,
                weight: 0,
            },
            Gene {
                source_type: SourceType::Sensor,
                source_num: 3,
                sink_type: SinkType::Neuron,
                sink_num: (params.max_neurons * 6) + 2,
                weight: 0,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: (params.max_neurons * 3) + 1,
                sink_type: SinkType::Neuron,
                sink_num: (params.max_neurons * 4) + 2,
                weight: 0,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: (params.max_neurons * 2) + 2,
                sink_type: SinkType::Action,
                sink_num: 3,
                weight: 0,
            },
        ];

        // All sinks without an action should be before any with
        let expected: Vec<NeuralNode> = vec![
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDist),
                sink: SinkNode::Neuron(1),
                weight: 0.0,
            },
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDistY),
                sink: SinkNode::Neuron(2),
                weight: 0.0,
            },
            NeuralNode {
                source: SourceNode::Neuron(1),
                sink: SinkNode::Neuron(2),
                weight: 0.0,
            },
            NeuralNode {
                source: SourceNode::Neuron(1),
                sink: SinkNode::Action(Action::MoveNorth),
                weight: 0.0,
            },
            NeuralNode {
                source: SourceNode::Neuron(2),
                sink: SinkNode::Action(Action::MoveSouth),
                weight: 0.0,
            },
        ];

        assert_eq!(Organism::build_neural_net(&genome, &params), expected);
    }

    #[test]
    fn test_action_ordering_neuron_without_action() -> () {
        let params = Params::default();
        let genome = vec![
            Gene {
                source_type: SourceType::Sensor,
                source_num: 1,
                sink_type: SinkType::Action,
                sink_num: 1,
                weight: 0,
            },
            // No action linked so invalid, should be removed
            Gene {
                source_type: SourceType::Sensor,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 1,
                weight: 0,
            },
        ];

        let expected: Vec<NeuralNode> = vec![NeuralNode {
            source: SourceNode::Sensor(Sensor::BoundaryDist),
            sink: SinkNode::Action(Action::MoveWest),
            weight: 0.0,
        }];

        assert_eq!(Organism::build_neural_net(&genome, &params), expected);
    }

    #[test]
    fn test_action_ordering_neuron_without_action_chain() -> () {
        let params = Params::default();
        let genome = vec![
            Gene {
                source_type: SourceType::Sensor,
                source_num: 1,
                sink_type: SinkType::Action,
                sink_num: 1,
                weight: i16::MAX - 1,
            },
            // No action linked so all invalid, should be removed
            Gene {
                source_type: SourceType::Sensor,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 1,
                weight: 2,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 2,
                weight: 3,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 2,
                sink_type: SinkType::Neuron,
                sink_num: 3,
                weight: 4,
            },
            Gene {
                source_type: SourceType::Sensor,
                source_num: 5,
                sink_type: SinkType::Neuron,
                sink_num: 2,
                weight: 5,
            },
            Gene {
                source_type: SourceType::Sensor,
                source_num: 7,
                sink_type: SinkType::Neuron,
                sink_num: 3,
                weight: 6,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 3,
                weight: 7,
            },
        ];

        let expected: Vec<NeuralNode> = vec![NeuralNode {
            source: SourceNode::Sensor(Sensor::BoundaryDist),
            sink: SinkNode::Action(Action::MoveWest),
            weight: 0.9999695,
        }];

        assert_eq!(Organism::build_neural_net(&genome, &params), expected);
    }

    #[test]
    fn test_action_ordering_neuron_loop_invalid() -> () {
        let params = Params::default();
        let genome = vec![
            Gene {
                source_type: SourceType::Sensor,
                source_num: Sensor::Age.to_u8(),
                sink_type: SinkType::Action,
                sink_num: 1,
                weight: 0,
            },
            // Long loop which is self referrential and no action so removing
            Gene {
                source_type: SourceType::Sensor,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 1,
                weight: 2,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 2,
                weight: 3,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 2,
                sink_type: SinkType::Neuron,
                sink_num: 3,
                weight: 4,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 3,
                sink_type: SinkType::Neuron,
                sink_num: 1,
                weight: 5,
            },
            // Single gene loop which is self referrential and no action so removing
            Gene {
                source_type: SourceType::Neuron,
                source_num: 4,
                sink_type: SinkType::Neuron,
                sink_num: 4,
                weight: 6,
            },
        ];

        let expected: Vec<NeuralNode> = vec![NeuralNode {
            source: SourceNode::Sensor(Sensor::Age),
            sink: SinkNode::Action(Action::MoveWest),
            weight: 0.0,
        }];

        assert_eq!(Organism::build_neural_net(&genome, &params), expected);
    }

    #[test]
    fn test_action_ordering_neuron_loop_valid() -> () {
        let params = Params::default();
        let genome = genome_long_neuron_list();
        let expected: Vec<NeuralNode> = vec![
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDist),
                sink: SinkNode::Neuron(1),
                weight: -0.33332315,
            },
            NeuralNode {
                source: SourceNode::Neuron(1),
                sink: SinkNode::Neuron(2),
                weight: 0.0,
            },
            NeuralNode {
                source: SourceNode::Neuron(2),
                sink: SinkNode::Neuron(3),
                weight: 0.33332315,
            },
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDist),
                sink: SinkNode::Action(Action::MoveWest),
                weight: -1.0000305,
            },
            NeuralNode {
                source: SourceNode::Neuron(3),
                sink: SinkNode::Action(Action::MoveNorth),
                weight: 1.0,
            },
        ];

        assert_eq!(Organism::build_neural_net(&genome, &params), expected);
    }

    #[test]
    fn test_sensor_age() -> () {
        // Converts age (units of simSteps compared to life expectancy)
        // linearly to normalized sensor range 0.0..1.0
        let habitat = Habitat::default();
        let params = Params::default(); // Default steps per gen = 300
        let test_subject = Organism::new(Point { x: 0, y: 0 }, &params);

        struct TestAge {
            age: u32,
            expected: f32,
        }
        let tests = vec![
            TestAge {
                age: 0,
                expected: 0.0,
            },
            TestAge {
                age: 50,
                expected: 0.166666667,
            },
            TestAge {
                age: 75,
                expected: 0.25,
            },
            TestAge {
                age: 150,
                expected: 0.5,
            },
            TestAge {
                age: 222,
                expected: 0.74,
            },
            TestAge {
                age: 300,
                expected: 1.0,
            },
        ];

        for t in tests {
            let value = test_subject.sensor_value(Sensor::Age, &habitat, &params, t.age);
            println!(
                "X: {}  Y: {}  Expected: {}  Got: {}",
                0, 0, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }
    struct TestLoc {
        loc: Point,
        expected: f32,
    }

    impl TestLoc {
        fn sensor_population_check(
            &self,
            sensor: Sensor,
            habitat: &Habitat,
            params: &Params,
        ) -> () {
            let test_subject = Organism::new(self.loc, params);
            let value = test_subject.sensor_value(sensor, habitat, params, 0);
            println!(
                "Loc: {},{}  Radius: {}  Rounded: {}  Expected: {}  Got: {}",
                self.loc.x,
                self.loc.y,
                params.population_sensor_radius.radius,
                params.population_sensor_radius.rounded,
                self.expected,
                value
            );
            assert_eq!(value, self.expected);
        }
    }

    #[test]
    fn test_sensor_boundary_dist() -> () {
        // Finds closest boundary, compares that to the max possible dist
        // to a boundary from the center, and converts that linearly to the
        // sensor range 0.0..1.0
        let habitat = Habitat::default();
        let params = Params::default();
        let tests = vec![
            TestLoc {
                loc: Point { x: 0, y: 126 },
                expected: 0.0,
            },
            TestLoc {
                loc: Point { x: 31, y: 32 },
                expected: 0.492063496,
            },
            TestLoc {
                loc: Point { x: 63, y: 65 },
                expected: 0.984126992,
            },
            TestLoc {
                loc: Point { x: 95, y: 90 },
                expected: 0.507936512,
            },
        ];

        for t in tests {
            let test_subject = Organism::new(t.loc, &params);
            let value = test_subject.sensor_value(Sensor::BoundaryDist, &habitat, &params, 0);
            println!(
                "X: {}  Y: {}  Expected: {}  Got: {}",
                t.loc.x, t.loc.y, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    #[test]
    fn test_sensor_boundary_dist_x() -> () {
        // Measures the distance to nearest boundary in the east-west axis,
        // max distance is half the grid width; scaled to sensor range 0.0..1.0.
        let habitat = Habitat::default();
        let params = Params::default();
        let tests = vec![
            TestLoc {
                loc: Point { x: 0, y: 30 },
                expected: 0.0,
            },
            TestLoc {
                loc: Point { x: 127, y: 50 },
                expected: 0.0,
            },
            TestLoc {
                loc: Point { x: 63, y: 70 },
                expected: 1.0,
            },
            TestLoc {
                loc: Point { x: 64, y: 70 },
                expected: 1.0,
            },
        ];

        for t in tests {
            let test_subject = Organism::new(t.loc, &params);
            let value = test_subject.sensor_value(Sensor::BoundaryDistX, &habitat, &params, 0);
            println!(
                "X: {}  Y: {}  Expected: {}  Got: {}",
                t.loc.x, t.loc.y, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    #[test]
    fn test_sensor_boundary_dist_y() -> () {
        // Measures the distance to nearest boundary in the south-north axis,
        // max distance is half the grid height; scaled to sensor range 0.0..1.0.
        let habitat = Habitat::default();
        let params = Params::default();
        let tests = vec![
            TestLoc {
                loc: Point { x: 80, y: 0 },
                expected: 0.0,
            },
            TestLoc {
                loc: Point { x: 90, y: 127 },
                expected: 0.0,
            },
            TestLoc {
                loc: Point { x: 90, y: 1 },
                expected: 0.015873016,
            },
            TestLoc {
                loc: Point { x: 110, y: 31 },
                expected: 0.492063496,
            },
            TestLoc {
                loc: Point { x: 120, y: 79 },
                expected: 0.761904768,
            },
            TestLoc {
                loc: Point { x: 120, y: 95 },
                expected: 0.507936512,
            },
        ];

        for t in tests {
            let test_subject = Organism::new(t.loc, &params);
            let value = test_subject.sensor_value(Sensor::BoundaryDistY, &habitat, &params, 0);
            println!(
                "X: {}  Y: {}  Expected: {}  Got: {}",
                t.loc.x, t.loc.y, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    struct TestFacing {
        facing: Facing,
        expected: f32,
    }

    #[test]
    fn test_sensor_last_move_dir_x() -> () {
        // X component -1,0,1 maps to sensor values 0.0, 0.5, 1.0
        let habitat = Habitat::default();
        let params = Params::default();
        let mut test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        let tests = vec![
            TestFacing {
                facing: Facing::North,
                expected: 0.5,
            },
            TestFacing {
                facing: Facing::NorthEast,
                expected: 1.0,
            },
            TestFacing {
                facing: Facing::East,
                expected: 1.0,
            },
            TestFacing {
                facing: Facing::SouthEast,
                expected: 1.0,
            },
            TestFacing {
                facing: Facing::South,
                expected: 0.5,
            },
            TestFacing {
                facing: Facing::SouthWest,
                expected: 0.0,
            },
            TestFacing {
                facing: Facing::West,
                expected: 0.0,
            },
            TestFacing {
                facing: Facing::NorthWest,
                expected: 0.0,
            },
        ];

        for t in tests {
            test_subject.facing = t.facing;
            let value = test_subject.sensor_value(Sensor::LastMoveDirX, &habitat, &params, 0);
            println!(
                "Facing: {}  Expected: {}  Got: {}",
                t.facing, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    #[test]
    fn test_sensor_last_move_dir_y() -> () {
        // Y component -1,0,1 maps to sensor values 0.0, 0.5, 1.0
        let habitat = Habitat::default();
        let params = Params::default();
        let mut test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        let tests = vec![
            TestFacing {
                facing: Facing::North,
                expected: 1.0,
            },
            TestFacing {
                facing: Facing::NorthEast,
                expected: 1.0,
            },
            TestFacing {
                facing: Facing::East,
                expected: 0.5,
            },
            TestFacing {
                facing: Facing::SouthEast,
                expected: 0.0,
            },
            TestFacing {
                facing: Facing::South,
                expected: 0.0,
            },
            TestFacing {
                facing: Facing::SouthWest,
                expected: 0.0,
            },
            TestFacing {
                facing: Facing::West,
                expected: 0.5,
            },
            TestFacing {
                facing: Facing::NorthWest,
                expected: 1.0,
            },
        ];

        for t in tests {
            test_subject.facing = t.facing;
            let value = test_subject.sensor_value(Sensor::LastMoveDirY, &habitat, &params, 0);
            println!(
                "Facing: {}  Expected: {}  Got: {}",
                t.facing, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    #[test]
    fn test_sensor_loc_x() -> () {
        // Maps current X location 0..p.sizeX-1 to sensor range 0.0..1.0
        let habitat = Habitat::default();
        let params = Params::default();
        let tests = vec![
            TestLoc {
                loc: Point { x: 0, y: 50 },
                expected: 0.0,
            },
            TestLoc {
                loc: Point { x: 33, y: 70 },
                expected: 0.259842528,
            },
            TestLoc {
                loc: Point { x: 64, y: 90 },
                expected: 0.503937024,
            },
            TestLoc {
                loc: Point { x: 127, y: 120 },
                expected: 1.0,
            },
        ];

        for t in tests {
            let test_subject = Organism::new(t.loc, &params);
            let value = test_subject.sensor_value(Sensor::LocX, &habitat, &params, 0);
            println!(
                "X: {}  Y: {}  Expected: {}  Got: {}",
                t.loc.x, t.loc.y, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    #[test]
    fn test_sensor_loc_y() -> () {
        // Maps current Y location 0..p.sizeY-1 to sensor range 0.0..1.0
        let habitat = Habitat::default();
        let params = Params::default();
        let tests = vec![
            TestLoc {
                loc: Point { x: 80, y: 0 },
                expected: 0.0,
            },
            TestLoc {
                loc: Point { x: 90, y: 80 },
                expected: 0.62992128,
            },
            TestLoc {
                loc: Point { x: 100, y: 120 },
                expected: 0.94488192,
            },
            TestLoc {
                loc: Point { x: 110, y: 127 },
                expected: 1.0,
            },
        ];

        for t in tests {
            let test_subject = Organism::new(t.loc, &params);
            let value = test_subject.sensor_value(Sensor::LocY, &habitat, &params, 0);
            println!(
                "X: {}  Y: {}  Expected: {}  Got: {}",
                t.loc.x, t.loc.y, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    struct TestCurrStep {
        curr_step: u32,
        expected: f32,
    }

    #[test]
    fn test_sensor_osc1() -> () {
        // Maps the oscillator sine wave to sensor range 0.0..1.0;
        // cycles starts at simStep 0 for everybody.
        let habitat = Habitat::default();
        let params = Params::default();
        let test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        let tests = vec![
            TestCurrStep {
                curr_step: 0,
                expected: f32::sin(0.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 17,
                expected: f32::sin(17.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 34,
                expected: f32::sin(34.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 40,
                expected: f32::sin(40.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 50,
                expected: f32::sin(50.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 65,
                expected: f32::sin(65.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 190,
                expected: f32::sin(190.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 254,
                expected: f32::sin(254.0 / 34.0),
            },
            TestCurrStep {
                curr_step: 300,
                expected: f32::sin(300.0 / 34.0),
            },
        ];

        for t in tests {
            let value = test_subject.sensor_value(Sensor::Osc1, &habitat, &params, t.curr_step);
            println!(
                "Step: {}  Expected: {}  Got: {}",
                t.curr_step, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    fn populate_habitat(habitat: &mut Habitat) -> () {
        struct TestLocs {
            loc: Point,
        }

        let pos = vec![
            // pop_fwd tests
            TestLocs {
                loc: Point { x: 0, y: 0 },
            },
            TestLocs {
                loc: Point { x: 10, y: 10 },
            },
            TestLocs {
                loc: Point { x: 40, y: 40 },
            },
            // Cluster for radius, around 60, 60
            //    XXX
            //       XX
            //     X
            //  X  OXXX
            //      X
            //  XXXXX
            //     X
            TestLocs {
                loc: Point { x: 59, y: 57 },
            },
            TestLocs {
                loc: Point { x: 60, y: 57 },
            },
            TestLocs {
                loc: Point { x: 61, y: 57 },
            },
            TestLocs {
                loc: Point { x: 62, y: 58 },
            },
            TestLocs {
                loc: Point { x: 63, y: 58 },
            },
            TestLocs {
                loc: Point { x: 60, y: 59 },
            },
            TestLocs {
                loc: Point { x: 57, y: 60 },
            },
            TestLocs {
                loc: Point { x: 61, y: 60 },
            },
            TestLocs {
                loc: Point { x: 62, y: 60 },
            },
            TestLocs {
                loc: Point { x: 63, y: 60 },
            },
            TestLocs {
                loc: Point { x: 61, y: 61 },
            },
            TestLocs {
                loc: Point { x: 57, y: 62 },
            },
            TestLocs {
                loc: Point { x: 58, y: 62 },
            },
            TestLocs {
                loc: Point { x: 59, y: 62 },
            },
            TestLocs {
                loc: Point { x: 60, y: 62 },
            },
            TestLocs {
                loc: Point { x: 61, y: 62 },
            },
            TestLocs {
                loc: Point { x: 60, y: 63 },
            },
        ];
        let mut id = 0;

        for p in pos {
            habitat.attempt_organism_placement(id, p.loc);
            id += 1;
        }
    }

    struct TestLocFace {
        loc: Point,
        facing: Facing,
        expected: f32,
    }

    impl TestLocFace {
        fn sensor_population_check(
            &self,
            sensor: Sensor,
            habitat: &Habitat,
            params: &Params,
        ) -> () {
            let mut test_subject = Organism::new(self.loc, params);
            test_subject.facing = self.facing;
            let value = test_subject.sensor_value(sensor, habitat, params, 0);
            println!(
                "Loc: {},{}  Facing: {}  Radius: {}  Rounded: {}  Expected: {}  Got: {}",
                self.loc.x,
                self.loc.y,
                self.facing,
                params.population_sensor_radius.radius,
                params.population_sensor_radius.rounded,
                self.expected,
                value
            );
            assert_eq!(value, self.expected);
        }

        fn sensor_barrier_check(&self, sensor: Sensor, habitat: &Habitat, params: &Params) -> () {
            let mut test_subject = Organism::new(self.loc, params);
            test_subject.facing = self.facing;
            let value = test_subject.sensor_value(sensor, habitat, params, 0);
            println!(
                "Loc: {},{}  Facing: {}  Radius: {}  Expected: {}  Got: {}",
                self.loc.x,
                self.loc.y,
                self.facing,
                params.barrier_sensor_radius,
                self.expected,
                value
            );
            assert_eq!(value, self.expected);
        }
    }

    #[test]
    fn test_sensor_longprobe_pop_fwd() -> () {
        // Measures the distance to the nearest other individual in the
        // forward direction. If non found, returns the maximum sensor value.
        // Maps the result to the sensor range 0.0..1.0.
        let mut habitat = Habitat::default();
        let params = Params::default();
        populate_habitat(&mut habitat);

        let tests = vec![
            TestLocFace {
                loc: Point { x: 0, y: 10 },
                facing: Facing::South,
                expected: 10.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 0, y: 10 },
                facing: Facing::North,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 5, y: 5 },
                facing: Facing::SouthEast,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 5, y: 5 },
                facing: Facing::NorthEast,
                expected: 5.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 5, y: 5 },
                facing: Facing::NorthWest,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 5, y: 10 },
                facing: Facing::East,
                expected: 5.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 5, y: 10 },
                facing: Facing::West,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 80, y: 0 },
                facing: Facing::NorthWest,
                expected: 40.0 / 128.0,
            },
        ];

        for t in tests {
            let mut test_subject = Organism::new(t.loc, &params);
            test_subject.facing = t.facing;
            let value =
                test_subject.sensor_value(Sensor::LongProbePopulationFwd, &habitat, &params, 0);
            println!(
                "Loc: {},{}  Facing: {}  Expected: {}  Got: {}",
                t.loc.x, t.loc.y, t.facing, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    #[test]
    fn test_sensor_longprobe_bar_fwd() -> () {
        // Measures the distance to the nearest barrier in the forward
        // direction. If none found, returns the maximum sensor value.
        // Maps the result to the sensor range 0.0..1.0.
        let mut habitat = Habitat::default();
        let params = Params::default();

        habitat.create_barrier(Point { x: 50, y: 50 }, Point { x: 100, y: 100 });

        let tests = vec![
            TestLocFace {
                loc: Point { x: 0, y: 0 },
                facing: Facing::South,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 0 },
                facing: Facing::East,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 0 },
                facing: Facing::NorthWest,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 0 },
                facing: Facing::North,
                expected: 50.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 75, y: 120 },
                facing: Facing::South,
                expected: 20.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 75, y: 120 },
                facing: Facing::North,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 75, y: 120 },
                facing: Facing::NorthEast,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 75, y: 120 },
                facing: Facing::SouthEast,
                expected: 20.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 75, y: 120 },
                facing: Facing::SouthWest,
                expected: 20.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 120 },
                facing: Facing::SouthEast,
                expected: 20.0 / 128.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 120 },
                facing: Facing::SouthWest,
                expected: 1.0,
            },
        ];

        for t in tests {
            let mut test_subject = Organism::new(t.loc, &params);
            test_subject.facing = t.facing;
            let value =
                test_subject.sensor_value(Sensor::LongProbeBarrierFwd, &habitat, &params, 0);
            println!(
                "Loc: {},{}  Facing: {}  Expected: {}  Got: {}",
                t.loc.x, t.loc.y, t.facing, t.expected, value
            );
            assert_eq!(value, t.expected);
        }
    }

    #[test]
    fn test_sensor_population() -> () {
        // Returns population density in neighborhood converted linearly from
        // 0..100% to sensor range
        let mut habitat = Habitat::default();
        let mut params = Params::default();
        populate_habitat(&mut habitat);

        // default param 2 with rounding
        let tests = vec![
            TestLoc {
                loc: Point { x: 0, y: 0 },
                expected: 1.0 / 8.0,
            },
            TestLoc {
                loc: Point { x: 55, y: 60 },
                expected: 1.0 / 21.0,
            },
            TestLoc {
                loc: Point { x: 56, y: 60 },
                expected: 2.0 / 21.0,
            },
            TestLoc {
                loc: Point { x: 60, y: 60 },
                expected: 7.0 / 21.0,
            },
            TestLoc {
                loc: Point { x: 60, y: 55 },
                expected: 3.0 / 21.0,
            },
            TestLoc {
                loc: Point { x: 61, y: 55 },
                expected: 2.0 / 21.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::Population, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 2,
            rounded: false,
        };
        let tests = vec![
            TestLoc {
                loc: Point { x: 55, y: 60 },
                expected: 1.0 / 13.0,
            },
            TestLoc {
                loc: Point { x: 56, y: 60 },
                expected: 1.0 / 13.0,
            },
            TestLoc {
                loc: Point { x: 60, y: 60 },
                expected: 5.0 / 13.0,
            },
            TestLoc {
                loc: Point { x: 60, y: 55 },
                expected: 1.0 / 13.0,
            },
            TestLoc {
                loc: Point { x: 61, y: 55 },
                expected: 1.0 / 13.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::Population, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 1,
            rounded: false,
        };
        let tests = vec![
            TestLoc {
                loc: Point { x: 60, y: 60 },
                expected: 2.0 / 5.0,
            },
            TestLoc {
                loc: Point { x: 61, y: 60 },
                expected: 3.0 / 5.0,
            },
            TestLoc {
                loc: Point { x: 62, y: 60 },
                expected: 3.0 / 5.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::Population, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 0,
            rounded: true,
        };
        let tests = vec![
            TestLoc {
                loc: Point { x: 60, y: 60 },
                expected: 0.0 / 1.0,
            },
            TestLoc {
                loc: Point { x: 61, y: 60 },
                expected: 1.0 / 1.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::Population, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 0,
            rounded: false,
        };
        let tests = vec![
            TestLoc {
                loc: Point { x: 60, y: 60 },
                expected: 0.0 / 1.0,
            },
            TestLoc {
                loc: Point { x: 61, y: 60 },
                expected: 1.0 / 1.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::Population, &habitat, &params);
        }
    }

    #[test]
    fn test_sensor_population_fwd() -> () {
        // Sense population density along axis of last movement direction, mapped
        // to sensor range 0.0..1.0
        let mut habitat = Habitat::default();
        let mut params = Params::default();
        populate_habitat(&mut habitat);

        // default param 2 with rounding
        let tests = vec![
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::North,
                expected: 1.0 / 7.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthEast,
                expected: 3.0 / 8.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::East,
                expected: 3.0 / 7.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::SouthEast,
                expected: 5.0 / 8.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::South,
                expected: 4.0 / 7.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::SouthWest,
                expected: 2.0 / 8.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::West,
                expected: 0.0 / 7.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthWest,
                expected: 1.0 / 8.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::PopulationFwd, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 2,
            rounded: false,
        };
        let tests = vec![
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::North,
                expected: 1.0 / 5.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthEast,
                expected: 3.0 / 6.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::East,
                expected: 3.0 / 5.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::SouthEast,
                expected: 4.0 / 6.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::South,
                expected: 2.0 / 5.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::SouthWest,
                expected: 1.0 / 6.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::West,
                expected: 0.0 / 5.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthWest,
                expected: 1.0 / 6.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::PopulationFwd, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 1,
            rounded: false,
        };
        let tests = vec![
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthEast,
                expected: 2.0 / 3.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::East,
                expected: 1.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::SouthEast,
                expected: 1.0 / 3.0,
            },
            TestLocFace {
                loc: Point { x: 61, y: 60 },
                facing: Facing::SouthEast,
                expected: 3.0 / 3.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::PopulationFwd, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 0,
            rounded: true,
        };
        let tests = vec![
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::North,
                expected: 0.0 / 1.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthEast,
                expected: 0.0 / 1.0,
            },
            TestLocFace {
                loc: Point { x: 61, y: 60 },
                facing: Facing::North,
                expected: 1.0 / 1.0,
            },
            TestLocFace {
                loc: Point { x: 61, y: 60 },
                facing: Facing::NorthEast,
                expected: 1.0 / 1.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::PopulationFwd, &habitat, &params);
        }
    }

    #[test]
    fn test_sensor_population_lr() -> () {
        // Sense population density along an axis 90 degrees from last movement direction
        let mut habitat = Habitat::default();
        let mut params = Params::default();
        populate_habitat(&mut habitat);

        // default param 2 with rounding
        let tests = vec![
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::North,
                expected: 3.0 / 13.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthEast,
                expected: 6.0 / 15.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::East,
                expected: 5.0 / 13.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::SouthEast,
                expected: 5.0 / 15.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::South,
                expected: 3.0 / 13.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::SouthWest,
                expected: 6.0 / 15.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::West,
                expected: 5.0 / 13.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthWest,
                expected: 5.0 / 15.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::PopulationSide, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 2,
            rounded: false,
        };
        let tests = vec![
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::North,
                expected: 3.0 / 9.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::East,
                expected: 3.0 / 9.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthEast,
                expected: 5.0 / 11.0,
            },
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::NorthWest,
                expected: 4.0 / 11.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::PopulationSide, &habitat, &params);
        }

        params.population_sensor_radius = SensorRadius {
            radius: 0,
            rounded: true,
        };
        let tests = vec![
            TestLocFace {
                loc: Point { x: 60, y: 60 },
                facing: Facing::North,
                expected: 0.0 / 1.0,
            },
            TestLocFace {
                loc: Point { x: 61, y: 60 },
                facing: Facing::North,
                expected: 1.0 / 1.0,
            },
        ];

        for t in tests {
            t.sensor_population_check(Sensor::PopulationSide, &habitat, &params);
        }
    }

    #[test]
    fn test_sensor_barrier_fwd() -> () {
        // Sense the nearest barrier along axis of last movement direction, mapped
        // to sensor range 0.0..1.0
        let mut habitat = Habitat::default();
        let mut params = Params::default();

        habitat.create_barrier(Point { x: 20, y: 20 }, Point { x: 40, y: 40 });
        habitat.create_barrier(Point { x: 25, y: 45 }, Point { x: 35, y: 55 });

        // default param 4
        let tests = vec![
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::East,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::NorthWest,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::North,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::NorthEast,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::South,
                expected: 0.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::SouthEast,
                expected: 0.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::South,
                expected: 2.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::North,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::SouthWest,
                expected: 2.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::West,
                expected: 1.0,
            },
        ];

        for t in tests {
            t.sensor_barrier_check(Sensor::ShortProbeBarrierFwd, &habitat, &params);
        }

        params.barrier_sensor_radius = 2;
        let tests = vec![
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::South,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::NorthEast,
                expected: 1.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::South,
                expected: 0.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::NorthWest,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::South,
                expected: 2.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::West,
                expected: 1.0,
            },
        ];

        for t in tests {
            t.sensor_barrier_check(Sensor::ShortProbeBarrierFwd, &habitat, &params);
        }
    }

    #[test]
    fn test_sensor_barrier_lr() -> () {
        // Sense the nearest barrier along axis perpendicular to last movement direction, mapped
        // to sensor range 0.0..1.0
        let mut habitat = Habitat::default();
        let mut params = Params::default();

        habitat.create_barrier(Point { x: 20, y: 20 }, Point { x: 40, y: 40 });
        habitat.create_barrier(Point { x: 25, y: 45 }, Point { x: 35, y: 55 });

        // default param 4
        let tests = vec![
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::East,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::NorthWest,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::North,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::NorthEast,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::West,
                expected: 0.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::SouthEast,
                expected: 0.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::South,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::East,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::West,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::SouthWest,
                expected: 1.0 / 4.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::North,
                expected: 1.0,
            },
        ];

        for t in tests {
            t.sensor_barrier_check(Sensor::ShortProbeBarrierLeftRight, &habitat, &params);
        }

        params.barrier_sensor_radius = 2;
        let tests = vec![
            TestLocFace {
                loc: Point { x: 20, y: 17 },
                facing: Facing::West,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::East,
                expected: 1.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 20, y: 18 },
                facing: Facing::NorthEast,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::North,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::West,
                expected: 0.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 41 },
                facing: Facing::NorthEast,
                expected: 0.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 44 },
                facing: Facing::East,
                expected: 0.0 / 2.0,
            },
            TestLocFace {
                loc: Point { x: 30, y: 43 },
                facing: Facing::South,
                expected: 1.0,
            },
            TestLocFace {
                loc: Point { x: 38, y: 48 },
                facing: Facing::South,
                expected: 1.0,
            },
        ];

        for t in tests {
            t.sensor_barrier_check(Sensor::ShortProbeBarrierLeftRight, &habitat, &params);
        }
    }

    #[test]
    fn test_sensor_random() -> () {
        // Returns a random sensor value in the range 0.0..1.0.
        let habitat = Habitat::default();
        let params = Params::default();
        let test_subject = Organism::new(Point { x: 0, y: 0 }, &params);

        let num_tests = 100;
        let mut out_of_bounds = false;
        let mut total: f32 = 0.0;

        for _ in 0..num_tests {
            let value = test_subject.sensor_value(Sensor::Random, &habitat, &params, 0);
            total += value;
            if value > 1.0 || value < 0.0 {
                out_of_bounds = true;
            }
        }
        assert_eq!(out_of_bounds, false);
        let average = total / num_tests as f32;
        assert!((0.4 < average) && (average < 0.6));
    }

    #[test]
    fn test_sensor_signal0() -> () {
        // Returns magnitude of signal0 in the local neighborhood, with
        // 0.0..maxSignalSum converted to sensorRange 0.0..1.0
        let habitat = Habitat::default();
        let params = Params::default();
        let test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        assert_eq!(
            test_subject.sensor_value(Sensor::Signal0, &habitat, &params, 0),
            INACTIVE_SENSOR_RESPONSE
        );
    }

    #[test]
    fn test_sensor_signal0_fwd() -> () {
        // Sense signal0 density along axis of last movement direction
        let habitat = Habitat::default();
        let params = Params::default();
        let test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        assert_eq!(
            test_subject.sensor_value(Sensor::Signal0Fwd, &habitat, &params, 0),
            INACTIVE_SENSOR_RESPONSE
        );
    }

    #[test]
    fn test_sensor_signal0_lr() -> () {
        // Sense signal0 density along an axis perpendicular to last movement direction
        let habitat = Habitat::default();
        let params = Params::default();
        let test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        assert_eq!(
            test_subject.sensor_value(Sensor::Signal0Side, &habitat, &params, 0),
            INACTIVE_SENSOR_RESPONSE
        );
    }

    #[test]
    fn test_sensor_genetic_sim_fwd() -> () {
        // Return minimum sensor value if nobody is alive in the forward adjacent location,
        // else returns a similarity match in the sensor range 0.0..1.0
        let habitat = Habitat::default();
        let params = Params::default();
        let test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        assert_eq!(
            test_subject.sensor_value(Sensor::GeneticSimilarityFwd, &habitat, &params, 0),
            INACTIVE_SENSOR_RESPONSE
        );
    }

    #[test]
    fn test_action_priorities_neuron_values_only() -> () {
        let habitat = Habitat::default();
        let params = Params::default();
        let mut test_subject = Organism::new(Point { x: 0, y: 0 }, &params);
        // N0 -> N1, N1 -> N2, N2 -> N3, N2 -> Action, N3 -> N0
        let genome = vec![
            Gene {
                source_type: SourceType::Neuron,
                source_num: 0,
                sink_type: SinkType::Neuron,
                sink_num: 1,
                weight: 1,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 1,
                sink_type: SinkType::Neuron,
                sink_num: 2,
                weight: 2,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 2,
                sink_type: SinkType::Neuron,
                sink_num: 3,
                weight: 3,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 2,
                sink_type: SinkType::Action,
                sink_num: 1,
                weight: 4,
            },
            Gene {
                source_type: SourceType::Neuron,
                source_num: 3,
                sink_type: SinkType::Neuron,
                sink_num: 0,
                weight: 3,
            },
        ];
        test_subject.neural_net = Organism::build_neural_net(&genome, &params);
        test_subject.neuron_values = vec![1.0, 2.1, 3.4, 4.6, 5.9];

        let expected_neuron_values = vec![1.0, 1.0, 1.0, 1.0, 5.9];
        assert_eq!(
            test_subject.calc_action_priorities(&habitat, &params, 0),
            (vec![(Action::MoveWest, 1.0)], expected_neuron_values)
        );
    }

    #[test]
    fn test_action_priorities_boundry_dist_via_neurons() -> () {
        let habitat = Habitat::default();
        let params = Params::default();
        let mut test_subject = Organism::new(Point { x: 10, y: 20 }, &params);
        let genome = genome_long_neuron_list();
        test_subject.neural_net = Organism::build_neural_net(&genome, &params);

        // BoundaryDist (????) -> N1 -> N2 -> N3 -> MoveNorth
        let expected_action_value: f32 = 0.15873016;
        let expected_neuron_values = vec![
            0.0,
            expected_action_value,
            expected_action_value,
            expected_action_value,
            0.0,
        ];
        let expected_actions = vec![
            (Action::MoveWest, expected_action_value),
            (Action::MoveNorth, expected_action_value),
        ];
        assert_eq!(
            test_subject.calc_action_priorities(&habitat, &params, 0),
            (expected_actions, expected_neuron_values)
        );
    }

    #[test]
    fn test_action_priorities_duplicate_action_maps() -> () {
        let habitat = Habitat::default();
        let params = Params::default();
        let mut test_subject = Organism::new(Point { x: 0, y: 0 }, &params);

        test_subject.neural_net = vec![
            NeuralNode {
                source: SourceNode::Sensor(Sensor::Age),
                sink: SinkNode::Action(Action::MoveRight),
                weight: 1.0,
            },
            NeuralNode {
                source: SourceNode::Sensor(Sensor::LocX),
                sink: SinkNode::Action(Action::MoveRight),
                weight: 0.5,
            },
            NeuralNode {
                source: SourceNode::Sensor(Sensor::BoundaryDist),
                sink: SinkNode::Action(Action::MoveRight),
                weight: -0.8,
            },
        ];

        let expected_actions = vec![
            (Action::MoveRight, 0.0),
            (Action::MoveRight, 0.0),
            (Action::MoveRight, 0.0),
        ];
        let expected_neuron_values = vec![0.0; 5];
        assert_eq!(
            test_subject.calc_action_priorities(&habitat, &params, 0),
            (expected_actions, expected_neuron_values)
        );
    }
}
