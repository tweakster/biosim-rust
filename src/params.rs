#[derive(Clone, Copy)]
pub struct SensorRadius {
    pub radius: u32,
    pub rounded: bool,
    // With rounded corners e.g.
    // 2 without:   X     2 with:  XXX
    //             XXX            XXXXX
    //            XXOXX           XXOXX
    //             XXX            XXXXX
    //              X              XXX
}

pub struct Params {
    pub barrier_sensor_radius: u32,
    pub default_osc_period: u32,
    pub gen_starting_pop: u32,
    pub genome_length: usize,
    pub max_neurons: u8,
    pub population_sensor_radius: SensorRadius,
    pub steps_per_gen: u32,
}

impl Params {
    pub fn default() -> Params {
        Params {
            barrier_sensor_radius: 4,
            default_osc_period: 34,
            gen_starting_pop: 1000,
            genome_length: 24,
            max_neurons: 5,
            population_sensor_radius: SensorRadius {
                radius: 2,
                rounded: true,
            },
            steps_per_gen: 300,
        }
    }
}
