use rand::Rng;
use wasm_bindgen::prelude::*;

use habitat::Habitat;
use organism::Organism;
use params::Params;
use point::Point;

mod facing;
mod habitat;
mod neighbourhood;
mod organism;
mod params;
mod point;

#[wasm_bindgen]
pub struct Simulation {
    params: Params,
    habitat: Habitat,
    organisms: Vec<Organism>,
    curr_step: u32,
    gen_num: u32,
    canvas: Vec<u32>,
}

#[wasm_bindgen]
impl Simulation {
    pub fn new() -> Simulation {
        Simulation {
            params: Params::default(),
            habitat: Habitat::default(),
            organisms: Vec::with_capacity(0),
            curr_step: 0,
            gen_num: 0,
            canvas: Vec::with_capacity(0),
        }
    }

    pub fn canvas_width(&self) -> u32 {
        self.habitat.width
    }

    pub fn canvas_height(&self) -> u32 {
        self.habitat.height
    }

    pub fn canvas_ptr(&self) -> *const u32 {
        self.canvas.as_ptr()
    }

    pub fn canvas_redraw(&mut self) {
        self.canvas = Vec::with_capacity(self.organisms.len() * 3);

        for o in &(self.organisms) {
            self.canvas.push(o.x());
            self.canvas.push(o.y());
            self.canvas.push(o.color());
        }
    }

    pub fn generation_num(&self) -> u32 {
        self.gen_num
    }

    pub fn num_organisms(&self) -> u32 {
        self.organisms.len() as u32
    }

    pub fn tick(&mut self) {
        if self.curr_step == 0 {
            self.populate_new_generation();
        }

        self.curr_step += 1;
        if self.curr_step > self.params.steps_per_gen {
            self.evaluate_organism_options();
            self.curr_step = 0;
            self.gen_num += 1;
        }
    }

    fn populate_new_generation(&mut self) {
        self.organisms = Vec::with_capacity(self.params.gen_starting_pop as usize);

        for i in 0..self.params.gen_starting_pop {
            let mut space_found = false;
            let mut loc = Point { x: 0, y: 0 };

            while !space_found {
                loc.x = rand::thread_rng().gen_range(0..self.habitat.width);
                loc.y = rand::thread_rng().gen_range(0..self.habitat.height);

                space_found = self.habitat.attempt_organism_placement(i, loc);
            }

            self.organisms.push(Organism::new(loc, &(self.params)));
        }
    }

    fn evaluate_organism_options(&mut self) {
        for o in &mut self.organisms {
            let (actions, neuron_values) =
                o.calc_action_priorities(&(self.habitat), &(self.params), self.curr_step);
            o.set_neuron_values(&neuron_values);
        }
    }
}
