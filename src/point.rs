use crate::facing::Facing;

#[cfg_attr(test, derive(Debug))]
#[derive(Clone, Copy, PartialEq)]
pub struct Point {
    pub x: u32,
    pub y: u32,
}

impl Point {
    pub fn advance(&mut self, facing: Facing) -> bool {
        let new_x: i64 = self.x as i64 + facing.x() as i64;
        let new_y: i64 = self.y as i64 + facing.y() as i64;

        if new_x < 0 || new_y < 0 || new_x > u32::MAX as i64 || new_y > u32::MAX as i64 {
            return false;
        }

        self.x = new_x as u32;
        self.y = new_y as u32;
        true
    }
}
