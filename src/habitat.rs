use crate::point::Point;

#[derive(Clone)]
enum Cell {
    Empty,
    Barrier,
    Organism(u32),
}

impl Cell {
    fn is_barrier(&self) -> bool {
        match self {
            Cell::Barrier => true,
            _ => false,
        }
    }

    fn is_empty(&self) -> bool {
        match self {
            Cell::Empty => true,
            _ => false,
        }
    }

    fn is_populated(&self) -> bool {
        match self {
            Cell::Organism(_) => true,
            _ => false,
        }
    }
}

pub struct Habitat {
    cells: Vec<Cell>,
    pub width: u32,
    pub height: u32,
}

impl Habitat {
    pub fn default() -> Habitat {
        let width: u32 = 128;
        let height: u32 = 128;
        let num_cells: usize = (width * height) as usize;

        Habitat {
            cells: vec![Cell::Empty; num_cells],
            width,
            height,
        }
    }

    // If the space is empty set id and return true. Else return false
    pub fn attempt_organism_placement(&mut self, id: u32, cell: Point) -> bool {
        let cell: Option<&mut Cell> = self.get_cell_mut(cell);
        match cell {
            Some(c) => {
                if c.is_empty() {
                    *c = Cell::Organism(id);
                    true
                } else {
                    false
                }
            }
            None => false,
        }
    }

    pub fn is_barrier(&self, cell: Point) -> bool {
        let cell: Option<&Cell> = self.get_cell(cell);
        match cell {
            Some(c) => {
                if c.is_barrier() {
                    true
                } else {
                    false
                }
            }
            None => false,
        }
    }

    pub fn is_populated(&self, cell: Point) -> bool {
        let cell: Option<&Cell> = self.get_cell(cell);
        match cell {
            Some(c) => {
                if c.is_populated() {
                    true
                } else {
                    false
                }
            }
            None => false,
        }
    }

    pub fn probe_is_barrier(&self, cell: Point) -> Option<bool> {
        let cell: Option<&Cell> = self.get_cell(cell);
        match cell {
            Some(c) => match c {
                Cell::Barrier => Some(true),
                _ => Some(false),
            },
            None => None,
        }
    }

    pub fn probe_is_populated(&self, cell: Point) -> Option<bool> {
        let cell: Option<&Cell> = self.get_cell(cell);
        match cell {
            Some(c) => match c {
                Cell::Empty => Some(false),
                Cell::Barrier => None,
                Cell::Organism(_) => Some(true),
            },
            None => None,
        }
    }

    pub fn create_barrier(&mut self, top_left: Point, bottom_right: Point) -> () {
        for x in top_left.x..(bottom_right.x + 1) {
            for y in top_left.y..(bottom_right.y + 1) {
                let cell: Option<&mut Cell> = self.get_cell_mut(Point { x, y });
                match cell {
                    Some(c) => {
                        *c = Cell::Barrier;
                    }
                    None => (),
                }
            }
        }
    }

    fn cell_vec_pos(cell: &Point, width: u32) -> Option<usize> {
        if cell.x >= width {
            return None;
        }
        Some((cell.y as usize * width as usize) + cell.x as usize)
    }

    fn get_cell(&self, cell: Point) -> Option<&Cell> {
        match Self::cell_vec_pos(&cell, self.width) {
            Some(p) => self.cells.get(p),
            None => None,
        }
    }

    fn get_cell_mut(&mut self, cell: Point) -> Option<&mut Cell> {
        match Self::cell_vec_pos(&cell, self.width) {
            Some(p) => self.cells.get_mut(p),
            None => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /*

    y

    a
    x
    i
    s

    5
    4   X  XXX
    3      XXX
    2 XXX  XXX
    1 XXX  XXX
    0 XXX
     0123456789  <- x axis
    */

    fn test_habitat() -> Habitat {
        let mut habitat = Habitat::default();
        habitat.create_barrier(Point { x: 1, y: 0 }, Point { x: 3, y: 2 });
        habitat.create_barrier(Point { x: 3, y: 4 }, Point { x: 3, y: 4 });
        habitat.create_barrier(Point { x: 6, y: 1 }, Point { x: 8, y: 4 });

        return habitat;
    }

    struct PointTest {
        point: Point,
        expected: bool,
    }

    struct ProbeTest {
        point: Point,
        expected: Option<bool>,
    }

    #[test]
    fn test_in_bounds() -> () {
        // default habitat is 128 x 128 i.e. 0->127 x 0->127
        let habitat = test_habitat();

        // expected true is in bounds
        let tests = vec![
            PointTest {
                point: Point { x: 0, y: 0 },
                expected: true,
            },
            PointTest {
                point: Point { x: 1, y: 1 },
                expected: true,
            },
            PointTest {
                point: Point { x: 128, y: 0 },
                expected: false,
            },
            PointTest {
                point: Point { x: 0, y: 128 },
                expected: false,
            },
            PointTest {
                point: Point { x: 50, y: 500 },
                expected: false,
            },
            PointTest {
                point: Point { x: u32::MAX, y: 0 },
                expected: false,
            },
            PointTest {
                point: Point { x: u32::MAX, y: 20 },
                expected: false,
            },
            PointTest {
                point: Point { x: 20, y: u32::MAX },
                expected: false,
            },
        ];
        for test in tests {
            println!("({},{}), {}", test.point.x, test.point.y, test.expected);
            let cell = habitat.get_cell(test.point);
            match cell {
                Some(_) => assert_eq!(true, test.expected),
                None => assert_eq!(false, test.expected),
            };
        }
    }

    #[test]
    fn test_organism_placement() -> () {
        let mut habitat = test_habitat();

        let tests = vec![
            PointTest {
                point: Point { x: 0, y: 0 },
                expected: true,
            },
            PointTest {
                point: Point { x: 0, y: 0 },
                expected: false,
            },
            PointTest {
                point: Point { x: 1, y: 1 },
                expected: false,
            },
            PointTest {
                point: Point { x: 1, y: 1 },
                expected: false,
            },
            PointTest {
                point: Point { x: 128, y: 0 },
                expected: false,
            },
            PointTest {
                point: Point { x: 64, y: 128 },
                expected: false,
            },
            PointTest {
                point: Point { x: 128, y: 128 },
                expected: false,
            },
        ];
        for test in tests {
            println!("({},{}), {}", test.point.x, test.point.y, test.expected);
            assert_eq!(
                habitat.attempt_organism_placement(0, test.point),
                test.expected
            );
        }
    }

    #[test]
    fn test_is_barrier() -> () {
        let habitat = test_habitat();

        // expected true is barrier
        let tests = vec![
            PointTest {
                point: Point { x: 0, y: 0 },
                expected: false,
            },
            PointTest {
                point: Point { x: 0, y: 1 },
                expected: false,
            },
            PointTest {
                point: Point { x: 1, y: 0 },
                expected: true,
            },
            PointTest {
                point: Point { x: 3, y: 2 },
                expected: true,
            },
            PointTest {
                point: Point { x: 4, y: 2 },
                expected: false,
            },
            PointTest {
                point: Point { x: 5, y: 2 },
                expected: false,
            },
            PointTest {
                point: Point { x: 6, y: 2 },
                expected: true,
            },
            PointTest {
                point: Point { x: 2, y: 3 },
                expected: false,
            },
            PointTest {
                point: Point { x: 3, y: 3 },
                expected: false,
            },
            PointTest {
                point: Point { x: 4, y: 3 },
                expected: false,
            },
            PointTest {
                point: Point { x: 2, y: 4 },
                expected: false,
            },
            PointTest {
                point: Point { x: 3, y: 4 },
                expected: true,
            },
            PointTest {
                point: Point { x: 4, y: 4 },
                expected: false,
            },
            PointTest {
                point: Point { x: 2, y: 5 },
                expected: false,
            },
            PointTest {
                point: Point { x: 3, y: 5 },
                expected: false,
            },
            PointTest {
                point: Point { x: 4, y: 5 },
                expected: false,
            },
        ];
        for test in tests {
            println!("({},{}), {}", test.point.x, test.point.y, test.expected);
            assert_eq!(habitat.is_barrier(test.point), test.expected);
        }
    }

    #[test]
    fn test_is_populated() -> () {
        let mut habitat = test_habitat();
        assert_eq!(
            habitat.attempt_organism_placement(0, Point { x: 4, y: 0 }),
            true
        );

        // expected true is populated
        let tests = vec![
            PointTest {
                point: Point { x: 3, y: 0 },
                expected: false,
            }, // barrier
            PointTest {
                point: Point { x: 4, y: 0 },
                expected: true,
            },
            PointTest {
                point: Point { x: 5, y: 0 },
                expected: false,
            }, // empty
        ];
        for test in tests {
            println!("({},{}), {}", test.point.x, test.point.y, test.expected);
            assert_eq!(habitat.is_populated(test.point), test.expected);
        }
    }

    #[test]
    fn test_probe_is_barrier() -> () {
        let habitat = test_habitat();

        let tests = vec![
            ProbeTest {
                point: Point { x: 2, y: 4 },
                expected: Some(false),
            },
            ProbeTest {
                point: Point { x: 3, y: 4 },
                expected: Some(true),
            },
            ProbeTest {
                point: Point { x: 129, y: 4 },
                expected: None,
            },
        ];
        for test in tests {
            println!("({},{}), {:?}", test.point.x, test.point.y, test.expected);
            assert_eq!(habitat.probe_is_barrier(test.point), test.expected);
        }
    }

    #[test]
    fn test_probe_is_populated() -> () {
        let mut habitat = test_habitat();
        assert_eq!(
            habitat.attempt_organism_placement(0, Point { x: 4, y: 0 }),
            true
        );

        let tests = vec![
            ProbeTest {
                point: Point { x: 2, y: 4 },
                expected: Some(false),
            },
            ProbeTest {
                point: Point { x: 4, y: 0 },
                expected: Some(true),
            },
            ProbeTest {
                point: Point { x: 129, y: 4 },
                expected: None,
            },
        ];
        for test in tests {
            println!("({},{}), {:?}", test.point.x, test.point.y, test.expected);
            assert_eq!(habitat.probe_is_populated(test.point), test.expected);
        }
    }
}
