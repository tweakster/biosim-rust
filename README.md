# Bio sim (Rust version)

## Intro

A version of David Miller's [biosim4](https://github.com/davidrmiller/biosim4).


## Development

The project uses wasm pack to build:
`cargo install wasm-pack`

To build it run
`wasm-pack build --target web`

It needs a web server to test. Try python's simple server
`python3 -m http.server`

And the page should be available on [http://127.0.0.1:8000/]