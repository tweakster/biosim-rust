import init, { Simulation } from "./pkg/biosim_rust.js";

const CELL_SIZE = 5; // px

init().then(wasm => {
  // Construct the simulation, and get its width and height.
  const simulation = Simulation.new();
  const width = simulation.canvas_width();
  const height = simulation.canvas_height();

  let generationNum = simulation.generation_num();
  let generationStartTime = performance.now();

  let redrawReq = true;

  const canvas = document.getElementById("sim-canvas");
  canvas.height = (CELL_SIZE) * height;
  canvas.width = (CELL_SIZE) * width;

  const ctx = canvas.getContext('2d');

  const drawCells = () => {
    ctx.beginPath();
    ctx.fillStyle = "#FFFFFF";
    ctx.fillRect(
      0, 0, width * CELL_SIZE, height * CELL_SIZE
    );

    simulation.canvas_redraw();
    const num_organisms = simulation.num_organisms();
    const orgPtr = simulation.canvas_ptr();
    const orgLocData = new Uint32Array(wasm.memory.buffer, orgPtr, num_organisms * 3);

    for (let o = 0; o < num_organisms; o++) {
      let idx = o * 3;

      let x = orgLocData[idx];
      let y = orgLocData[idx + 1];
      let color = orgLocData[idx + 2];

      ctx.fillStyle = "#" + color.toString(16).padStart(6, "0");

      ctx.fillRect(
        y * CELL_SIZE,
        x * CELL_SIZE,
        CELL_SIZE,
        CELL_SIZE
      );
    }
    redrawReq = false;
  };

  // The main loop of the simulation. Should be running almost continuously
  const mainLoop = () => {
    simulation.tick();

    if (redrawReq) {
      drawCells();

    }

    if ( generationNum != simulation.generation_num()) {
      generationNum = simulation.generation_num();
      let curr_time = performance.now();
      console.log(curr_time - generationStartTime);
      generationStartTime = curr_time;
    }

    setTimeout(mainLoop);
  };

  // Triggered every time a new frame of animation is required
  const newFrameLoop = () => {
    redrawReq = true;
    requestAnimationFrame(newFrameLoop);
  }

  drawCells();
  setTimeout(mainLoop);
  requestAnimationFrame(newFrameLoop);
});
